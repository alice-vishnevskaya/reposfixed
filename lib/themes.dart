import 'package:flutter/material.dart';

class LightTheme {
   static const backgroundColor = const Color(0xffffffff);
   //static const textboxColor = const Color(0xff4db6ac);
   static const textColor = const Color(0x000000ff);
}