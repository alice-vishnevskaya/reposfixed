import 'package:flutter/material.dart';

void move(BuildContext context, Widget next())
{
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => next()),
  );
}