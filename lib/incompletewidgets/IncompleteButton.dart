//import 'dart:ffi';
import 'package:flutter/material.dart';

Widget IncompleteButton(String text, {double size, int iconind , void todo()}) {
    return Container( 
    margin: EdgeInsets.symmetric(horizontal : 7 , vertical : 10),
    child: InkWell(
        
      onTap: () {
        todo();
      },
      child: Container(
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(iconind == null ? 12 : 40),color: Color(0xff79B7F0)),
      child: Ink(
      //margin: EdgeInsets.symmetric(horizontal : 5 , vertical : 10),
      padding: EdgeInsets.only(top: 20, bottom: 20, left: 15, right: 15),
      width : size != null ? size : double.infinity,
      child: Container(
        child: Row(
          
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,

          children: <Widget>[

            // Container(
            //   padding: EdgeInsets.only(left: 0, top: 10, bottom: 10),
            //   child: Icon(Icons.keyboard_arrow_left, color: Colors.black),
            // ),
            iconind == null ? 
            Text(text,
                style: TextStyle(fontSize: 20, fontWeight: FontWeight.w500, color: Colors.white,)
            )
            :
            Icon( Icons.arrow_back , color: Colors.white,),

          ],
        ),
      ),
    ))
    )
    );
  }