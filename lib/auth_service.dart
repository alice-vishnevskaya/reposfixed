import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:background_location/background_location.dart';

class AuthenticationService {

  final FirebaseAuth _firebaseAuth;

  AuthenticationService(this._firebaseAuth);

  Stream<User> get authStateChanges => _firebaseAuth.authStateChanges();

   Future<String> signIn(String email, String password) async{
    try {
      await _firebaseAuth.signInWithEmailAndPassword(email: email, password: password);
     
      return "You are now signed in";
    } on FirebaseAuthException catch(e){
      return e.message;
    }
  }

  Future<String> signUp(String email, String password, String username) async{
    try {
      await _firebaseAuth.createUserWithEmailAndPassword(email: email, password: password);
      
      double latitude, longitude;

      BackgroundLocation.getLocationUpdates((location) {
        latitude = location.latitude;
        longitude = location.longitude;
        //time = DateTime.fromMillisecondsSinceEpoch(location.time.toInt()).toString();
    });

      FirebaseFirestore.instance.collection('users' ).
      doc(_firebaseAuth.currentUser.uid).set({
        'Location':[latitude, longitude],
        'email':email, 
        'username': username.length == 0 ? 'Untitled User' : username,
        'online': false,
        'busy' : false, 
        'busyname' : username.length == 0 ? 'Untitled User' : username,
        'friends' : FieldValue.arrayUnion([]),
        'pending' : FieldValue.arrayUnion([]),
        'groups' : FieldValue.arrayUnion([]),
        });
      return "You are now signed up";
    } on FirebaseAuthException catch(e){
      return e.message;
    }
  }


  Future<void> signOut() async {
    await _firebaseAuth.signOut();
  }
}