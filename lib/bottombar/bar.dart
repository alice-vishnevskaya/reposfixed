import 'dart:math';
import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:incomplete_app/bottombar/content/page_groups.dart';

//import 'package:outline_material_icons/outline_material_icons.dart';
//

import 'dart:ui';

import 'package:flutter/material.dart';
import 'dart:async';
import 'dart:isolate';

import 'package:background_locator/location_dto.dart';
import 'package:background_locator/settings/android_settings.dart';
import 'package:background_locator/settings/ios_settings.dart';
import 'package:background_locator/settings/locator_settings.dart';
import 'package:flutter/rendering.dart';

import 'package:location_permissions/location_permissions.dart';


import 'content/page_groups.dart';
import 'content/page_home.dart';
import 'content/page_profile.dart';
import 'content/page_groups.dart';
import 'navbar.dart';
//import 'package:flutter/services.dart';
//import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:background_location/background_location.dart';

class StringReference{
  String value;
  StringReference(this.value);
}

class BubbleTabBar extends StatefulWidget {
  @override
  _BubbleTabBarState createState() => _BubbleTabBarState();
}

class _BubbleTabBarState extends State<BubbleTabBar> {
  List<NavBarItemData> _navBarItems;
  int _selectedNavIndex = 0;
  
  StringReference latitude = StringReference(' Searching people...');
  StringReference longitude = StringReference('');

  //VoidCallback stopTracking;

  //ReceivePort port = ReceivePort();

  final FirebaseAuth auth = FirebaseAuth.instance;
  int counter = 0;
  Timer _timer;

  updateCounter() {
    FirebaseFirestore.instance.collection('users').doc(auth.currentUser.uid).update(
      { 
        'online': true
      }
    );
    _timer = Timer.periodic(Duration(seconds: 3), (timer) {
      // You can also call here any function.
      //setState(() {
        _startLocator();
        counter = counter++;
        BackgroundLocation.stopLocationService();
      //});
    });
  }

  stopCounter() {
    _timer.cancel();
    FirebaseFirestore.instance.collection('users').doc(auth.currentUser.uid).update(
      { 
        'online': false
      }
    );
    onStop();
  }

  String logStr = '';
  bool isRunning;
  LocationDto lastLocation;
  DateTime lastTimeLocation;

  bool loggedOut = false;

  List<Widget> _viewsByIndex;

  @override
  void initState() {

    super.initState();

    //sleep(Duration(seconds: 5));

    _navBarItems = [
      NavBarItemData("Home", Icons.search_outlined, 120, Color(0xfff3f3f4)),
      NavBarItemData("Social", Icons.menu, 120, Color(0xfff3f3f4)),
      NavBarItemData("Profile", Icons.account_circle_outlined, 110, Color(0xfff3f3f4)),
    ];

    _viewsByIndex = <Widget>[//экраны
      Wrapper(latitude, longitude, updateCounter, stopCounter),//HomePage
      GroupsWrapper(),
      ProfileWrapper(stopCounter), //ProfilePage
    ];

    onStart();
    updateCounter();
    
    //  на старте давать локацию  
  }

  //НОВЫЙ ЛОКАТОР 

  void getCurrentLocation() {
    BackgroundLocation().getCurrentLocation().then((location) {
      print('This is current Location ' + location.toMap().toString());
    });
  }

  @override
  void dispose() {
    stopCounter();
    //BackgroundLocation.stopLocationService();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {


    var accentColor = _navBarItems[_selectedNavIndex].selectedColor;

    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    //Create custom navBar, pass in a list of buttons, and listen for tap event
    var navBar = NavBar(
      items: _navBarItems,
      itemTapped: _handleNavBtnTapped,
      currentIndex: _selectedNavIndex,
    );
    //Display the correct child view for the current index
    var contentView = _viewsByIndex[min(_selectedNavIndex, _viewsByIndex.length - 1)];
    //Wrap our custom navbar + contentView with the app Scaffold
    return Scaffold(
      backgroundColor: Color(0xffE6E6E6),
      body: SafeArea(
        child: Container(
          width: width,
          //Wrap the current page in an AnimatedSwitcher for an easy cross-fade effect
          child: AnimatedSwitcher(
            duration: Duration(milliseconds: 350),
            //Pass the current accent color down as a theme, so our overscroll indicator matches the btn color
            child: Theme(
              data: ThemeData(accentColor: accentColor),
              child: contentView,
            ),
          ),
        ),
      ),
      bottomNavigationBar: navBar, //Pass our custom navBar into the scaffold
    );
  }

   void onStop() async {

    loggedOut = true;
    BackgroundLocation.stopLocationService();

    //setState(() {
      isRunning = false;
      //lastTimeLocation = null;
      //lastLocation = null;
    //});
  }

  void onStart() async {
    if (await _checkLocationPermission()) {
      //дописал короб
      loggedOut = false;

      //setState(() {
        isRunning = true;
        //lastTimeLocation = null;
        //lastLocation = null;
      //});
    } else {
      //show error
    }
  }

  Future<bool> _checkLocationPermission() async {
    final access = await LocationPermissions().checkPermissionStatus();
    switch (access) {
      case PermissionStatus.unknown:
      case PermissionStatus.denied:
      case PermissionStatus.restricted:
        final permission = await LocationPermissions().requestPermissions(
          permissionLevel: LocationPermissionLevel.locationAlways,
        );
        if (permission == PermissionStatus.granted) {
          return true;
        } else {
          return false;
        }
        break;
      case PermissionStatus.granted:
        return true;
        break;
      default:
        return false;
        break;
    }
  }

  void _startLocator() async {
    
    await BackgroundLocation.setAndroidNotification(
                      title: 'Got your location',
                      message: '',
                      icon: '@mipmap/ic_launcher',
    );
    
    //await BackgroundLocation.setAndroidConfiguration(1000);
    await BackgroundLocation.startLocationService(distanceFilter: 20);
    BackgroundLocation.getLocationUpdates((location) {
      //setState(() {
        latitude.value = location.latitude.toString();
        longitude.value = location.longitude.toString();
        //time = DateTime.fromMillisecondsSinceEpoch(location.time.toInt()).toString();
      //});
    });
    FirebaseFirestore.instance.collection('users').doc(auth.currentUser.uid).update(
      { 
        'Location': [double.parse(latitude.value), double.parse(longitude.value)]
      }
    );
    print('''\n
      Latitude:  ${double.parse(latitude.value)}
      Longitude: ${double.parse(longitude.value)}
    ''');
  }

  void _handleNavBtnTapped(int index) {
    setState(() {
      _selectedNavIndex = index;
    });
  }
}


