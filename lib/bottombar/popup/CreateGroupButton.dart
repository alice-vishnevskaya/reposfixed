//import 'dart:html';

import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'custom_rect_tween.dart';
import 'hero_dialog_route.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

/// {@template add_todo_button}
/// Button to add a new [Todo].
///
/// Opens a [HeroDialogRoute] of [_AddTodoPopupCard].
///
/// Uses a [Hero] with tag [_heroAddTodo].
/// {@endtemplate}
class CreateGroupButton extends StatelessWidget {
  /// {@macro add_todo_button}
  CreateGroupButton(this.func, {Key key}) : super(key: key);
  final Function func;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(25.0),
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).push(HeroDialogRoute(builder: (context) {
            return _AddTodoPopupCard(func);
          }));
        },
        child: Hero(
          tag: _heroAddTodo,
          createRectTween: (begin, end) {
            return CustomRectTween(begin: begin, end: end);
          },
            child:Container(
              alignment: Alignment.bottomRight,
            child: Container(
              width: MediaQuery.of(context).size.width/6,
              height: MediaQuery.of(context).size.width/6,
              decoration: new BoxDecoration(
                color: Color(0xff79B7F0),
                shape: BoxShape.circle,
              ),
           child: Center(
             child: Icon(Icons.add, size: 30, color: Colors.white),
           )
          )),
          )
        ),
    );
  }
}


const String _heroAddTodo = 'add-todo-hero';

class _AddTodoPopupCard extends StatefulWidget {
  final Function func;
  _AddTodoPopupCard(this.func);
  @override
  _State createState() => _State(func);
}

class _State extends State<_AddTodoPopupCard> {
final Function func;
  _State(this.func);
final TextEditingController _titleController = TextEditingController();
final TextEditingController _infoController = TextEditingController();

void debugCallback(String text)
{
  Fluttertoast.showToast(
      msg: text,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIos: 1,
      backgroundColor: Colors.lightBlue,
      textColor: Colors.white
  );
}
  @override
  Widget build(BuildContext context) {
      return Center(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Hero(
          tag: _heroAddTodo,
          createRectTween: (begin, end) {
            return CustomRectTween(begin: begin, end: end);
          },
          child: Material(
            color: Colors.white,
            elevation: 2,
            shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TextField(
                      maxLength: 24,
                      controller: _titleController,
                    //  controller: _controller,
                      decoration: InputDecoration(
                        counterText: '',
                        hintText: 'Title goes here',
                        border: InputBorder.none,
                      ),
                      cursorColor: Colors.black,
                    ),
                    SizedBox(height:1.25),
                    Divider(
                      color: Colors.black,
                      thickness: 0.4,
                    ),
                    SizedBox(height:1.25),
                    TextField(
                      controller: _infoController,
                      //  controller: _controller,
                      decoration: InputDecoration(
                        hintText: 'Description goes here',
                        border: InputBorder.none,
                      ),
                      cursorColor: Colors.black,
                      maxLines: 5,
                    ),
                    SizedBox(height:5),
                    FlatButton(
                      onPressed: () async {
                        final groupid = await FirebaseFirestore.instance.collection('groups').add(
                          {
                            'title' : (_titleController.text.isNotEmpty)?_titleController.text:"No Name",
                            'info' : _infoController.text,
                            'people' : FieldValue.arrayUnion([FirebaseAuth.instance.currentUser.uid]),
                          }
                        ).then((value) => value.id);
                        FirebaseFirestore.instance.collection('users').doc(FirebaseAuth.instance.currentUser.uid).update(
                          {
                            'groups': FieldValue.arrayUnion([groupid]),
                          }
                        );
                        debugCallback("Group has been created succesfully!");
                        func();
                      },
                      child: const Text('Create'),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
} 
