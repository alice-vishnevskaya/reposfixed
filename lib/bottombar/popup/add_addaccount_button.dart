import 'package:flutter/material.dart';
import 'package:incomplete_app/bottombar/content/page_home.dart';
import 'package:incomplete_app/bottombar/popup/SingleChoicePlatform.dart';
import 'custom_rect_tween.dart';
import 'hero_dialog_route.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';

void addAccount(FirebaseAuth auth, String sNetwork, String accName){
    FirebaseFirestore.instance.collection('users').doc(auth.currentUser.uid).update(
      { 
        sNetwork: accName,
      }
    );
}

String textFieldInput;

class AddAccButton extends StatelessWidget {
  //final VoidCallback addAcc;
  const AddAccButton({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 12.0, top: 4, bottom: 10),
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).push(HeroDialogRoute(builder: (context) {
            return DropDown();
          }));
        },
        child: Hero(
          tag: _heroAddTodo,
          createRectTween: (begin, end) {
            return CustomRectTween(begin: begin, end: end);
          },
          child: //const Icon(IconData(0xe900, fontFamily: 'Pencil'), size: 25),
          Container(
            alignment: Alignment.centerLeft,
            //margin: EdgeInsets.only(right:12, left:12,top:4,bottom:10),
            decoration: BoxDecoration(
              color: Colors.grey,
              borderRadius: BorderRadius.circular(8.0),
            ),
            //fillColor: Color(0xffBDDEB5),
            //
            child: Row(
                mainAxisSize: MainAxisSize.min,
                //crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: EdgeInsets.only(top: 4, bottom: 4, right: 2, left: 7),
                    child: Icon(
                        Icons.add, color: Colors.white),
                  ),
                  Container( padding: EdgeInsets.only(right:8),
                      child: Text('add account ',
                        //textAlign: TextAlign.center,
                        style: TextStyle(fontWeight: FontWeight
                            .w500,
                            fontSize: 20,
                            color: Colors.white),)),

                ]
            ),
          )
        ),
      ),
    );
  }
}


const String _heroAddTodo = 'add-todo-hero';

class DropDown extends StatefulWidget {
  DropDown() : super();

  final String title = "DropDown Demo";

  @override
  DropDownState createState() => DropDownState();
}

class Company {
  int id;
  String name;

  Company(this.id, this.name);

  static List<Company> getCompanies() {
    return <Company>[
      Company(0, 'twitter'),
      Company(1, 'instagram'),
      Company(2, 'soundcloud'),
    ];
  }
}

class ChoicePlatform {
  List<Widget> Platform = [];
}

class DropDownState extends State<DropDown> {
  //
  final FirebaseAuth auth = FirebaseAuth.instance;
  List<Company> _companies = Company.getCompanies();

  final TextEditingController _controller = TextEditingController();

  List<DropdownMenuItem<Company>> _dropdownMenuItems;
  Company _selectedCompany;

  @override
  void initState() {
    _dropdownMenuItems = buildDropdownMenuItems(_companies);
    _selectedCompany = _dropdownMenuItems[0].value;
    super.initState();
    _controller.addListener(() {
      //final String accName = _controller.text.toLowerCase();
      _controller.value = _controller.value.copyWith(
        text: textFieldInput,
        selection:
            TextSelection(baseOffset: textFieldInput.length, extentOffset: textFieldInput.length),
        composing: TextRange.empty,
      );
    });
  }

  List<DropdownMenuItem<Company>> buildDropdownMenuItems(List companies) {
    List<DropdownMenuItem<Company>> items = List();
    for (Company company in companies) {
      items.add(
        DropdownMenuItem(
          value: company,
          child: Text(company.name),
        ),
      );
    }
    return items;
  }

  onChangeDropdownItem(Company selectedCompany) {
    setState(() {
      _selectedCompany = selectedCompany;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Center(
        child: Padding(
        padding: const EdgeInsets.all(10.0),
          child: Hero(
            tag: _heroAddTodo,
            createRectTween: (begin, end) {
           return CustomRectTween(begin: begin, end: end);
           },
            child: Material(
            color: Colors.white,
            elevation: 2,
            shape:
            RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
              child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.all(16.0),
                  child: Column(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Container(
                          child: Center(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Text("Select social network"),
                                SizedBox(
                                  height: 20.0,
                                ),
                                DropdownButton(
                                  value: _selectedCompany,
                                  items: _dropdownMenuItems,
                                  onChanged: onChangeDropdownItem,
                                ),
                                SizedBox(
                                  height: 20.0,
                                ),

                                Text('Selected: ${_selectedCompany.name}'),
                                SizedBox(
                                  height: 20.0,
                                ),
                                 TextField(
                                   maxLength: 25,
                                   controller: _controller,
                                  decoration: InputDecoration(
                                    hintText: '@${_selectedCompany.name}Example',
                                    border: InputBorder.none,
                                  ),
                                  cursorColor: Colors.black,
                                  maxLines: 1,
                                ),
                                 Divider(
                                  color: Colors.white,
                                  thickness: 0.2,
                                ),
                                FlatButton(
                                  onPressed: () {
                                    RegExp regex;
                                    if(_selectedCompany.name == 'twitter' || _selectedCompany.name == 'instagram')
                                    regex =new RegExp(r"^@[A-Za-z0-9_]{1,15}$");
                                    else
                                      regex = new RegExp(r"^[A-Za-z0-9-]{1,25}$");
                                    Iterable<Match> matches = regex.allMatches(_controller.text);
                                    if(matches.length>0) {
                                      setState(() {
                                        addAccount(auth, _selectedCompany.name,
                                            _controller.text);
                                      });
                                      debugCallback("Account has added succesfully!");
                                    }
                                    else
                                      {
                                        debugCallback("Incorrect name format!");
                                      }
                                  },
                                  child: const Text('Add'),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ]
    ),
    ),
    )))));
  }
}



