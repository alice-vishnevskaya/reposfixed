//import 'dart:html';

import 'package:flutter/material.dart';
import 'custom_rect_tween.dart';
import 'hero_dialog_route.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

/// {@template add_todo_button}
/// Button to add a new [Todo].
///
/// Opens a [HeroDialogRoute] of [_AddTodoPopupCard].
///
/// Uses a [Hero] with tag [_heroAddTodo].
/// {@endtemplate}
class JoinGroupButton extends StatelessWidget {
  /// {@macro add_todo_button}
  JoinGroupButton(this.func, {Key key}) : super(key: key);
  final Function func;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(25.0),
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).push(HeroDialogRoute(builder: (context) {
            return _AddTodoPopupCard(func);
          }));
        },
        child: Hero(
          tag: _heroAddTodo,
          createRectTween: (begin, end) {
            return CustomRectTween(begin: begin, end: end);
          },
            child:Container(
              alignment: Alignment.bottomLeft,
            child: Container(
              width: MediaQuery.of(context).size.width/6,
              height: MediaQuery.of(context).size.width/6,
              decoration: new BoxDecoration(
                color: Color(0xff79B7F0),
                shape: BoxShape.circle,
              ),
           child: Center(
             child: Icon(Icons.group_add, size: 30, color: Colors.white),
           )
          )),
          )
        ),
    );
  }
}


const String _heroAddTodo = 'add-todo-hero';

class _AddTodoPopupCard extends StatefulWidget {
  final Function func;
  _AddTodoPopupCard(this.func);
    @override
    State createState() => _State(func);
  }

  class _State extends State<_AddTodoPopupCard> {
  final Function func;
  _State(this.func);
  /// {@macro add_todo_popup_card}
  ///const _AddTodoPopupCard({Key key}) : super(key: key);
  
  final FirebaseAuth auth = FirebaseAuth.instance;
  final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
  
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Hero(
          tag: _heroAddTodo,
          createRectTween: (begin, end) {
            return CustomRectTween(begin: begin, end: end);
          },
          child: Material(
            color: Colors.white,
            elevation: 2,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TextField(
                        controller: _controller,
                      decoration: InputDecoration(
                        hintText: "Existing group's id goes here",
                        border: InputBorder.none,
                      ),
                      cursorColor: Colors.black,
                    ),
                    SizedBox(height:5),
                    FlatButton(
                      onPressed: () {
                        FirebaseFirestore.instance.collection('groups').doc(_controller.text).update(
                          {
                            'people': FieldValue.arrayUnion([FirebaseAuth.instance.currentUser.uid]),
                          }
                        );
                        FirebaseFirestore.instance.collection('users').doc(FirebaseAuth.instance.currentUser.uid).update(
                          {
                            'groups': FieldValue.arrayUnion([_controller.text]),
                          }
                        );
                        func();
                      },
                      child: const Text('Join'),
                    )
                  ]
              ),
            ),
          ),
        ),
      ),
    ));
  }
}
