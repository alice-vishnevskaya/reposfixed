import 'package:flutter/material.dart';
import 'package:incomplete_app/bottombar/content/page_profile.dart';
import 'custom_rect_tween.dart';
import 'hero_dialog_route.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fluttertoast/fluttertoast.dart';

void editName(FirebaseAuth auth, String newname, String busyname){
    if (newname.length == 0 && busyname.length == 0)
    Fluttertoast.showToast(
      msg: 'Fields are empty, nothing changed',
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIos: 2,
      backgroundColor: Colors.lightBlue,
      textColor: Colors.white
    );
    else Fluttertoast.showToast(
      msg: 'Saved',
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIos: 1,
      backgroundColor: Colors.lightBlue,
      textColor: Colors.white
    );
    if (newname.length != 0)
    FirebaseFirestore.instance.collection('users').doc(auth.currentUser.uid).update(
      { 
        'username': newname,
      }
    );
    if (busyname.length != 0)
    FirebaseFirestore.instance.collection('users').doc(auth.currentUser.uid).update(
      { 
        'busyname' : busyname,
      }
    );
}

String userName;

const String _heroAddTodo = 'add-todo-hero';

/// {@template add_todo_button}
/// Button to add a new [Todo].
///
/// Opens a [HeroDialogRoute] of [_AddTodoPopupCard].
///
/// Uses a [Hero] with tag [_heroAddTodo].
/// {@endtemplate}
class AddTodoButton extends StatelessWidget {
  /// {@macro add_todo_button}
  const AddTodoButton({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(0.0),
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).push(HeroDialogRoute(builder: (context) {
            return _AddTodoPopupCard();
          }));
        },
        child: Hero(
          tag: _heroAddTodo,
          createRectTween: (begin, end) {
            return CustomRectTween(begin: begin, end: end);
          },
            child: const Icon(IconData(0xe900, fontFamily: 'Pencil'), size: 25
            ),
          ),
      ),
    );
  }
}

  class _AddTodoPopupCard extends StatefulWidget {
    @override
    State createState() => _State();
  }

  class _State extends State<_AddTodoPopupCard> {
  /// {@macro add_todo_popup_card}
  ///const _AddTodoPopupCard({Key key}) : super(key: key);
  
  final FirebaseAuth auth = FirebaseAuth.instance;
  final TextEditingController _controller = TextEditingController();
  final TextEditingController _controller2 = TextEditingController();

  @override
  void initState() {
    super.initState();
    _controller.addListener(() {
      //_controller.text = uName;
      _controller.value = _controller.value.copyWith(
        text: userName,
        selection:
            TextSelection(baseOffset: userName.length, extentOffset: userName.length),
        composing: TextRange.empty,
      );
    });
  }

  @override
  Widget build(BuildContext context) {
  
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(10.0),
        child: Hero(
          tag: _heroAddTodo,
          createRectTween: (begin, end) {
            return CustomRectTween(begin: begin, end: end);
          },
          child: Material(
            color: Colors.white,
            elevation: 2,
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),
            child: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  children: [
                    TextField(
                      maxLength: 18,
                        controller: _controller,
                      decoration: InputDecoration(
                        counterText: '',
                        hintText: 'New nickname goes here',
                        border: InputBorder.none,
                      ),
                      cursorColor: Colors.black,
                    ),
                    SizedBox(height:2),
                    TextField(
                      maxLength: 35,
                        controller: _controller2,
                      decoration: InputDecoration(
                        counterText: '',
                        hintText: 'Busy mode name goes here',
                        border: InputBorder.none,
                      ),
                      cursorColor: Colors.black,
                    ),
                    SizedBox(height:5),
                    FlatButton(
                      onPressed: () {setState((){editName(auth, _controller.text, _controller2.text);});},
                      child: const Text('Save'),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
      ),
    );
  }
}
