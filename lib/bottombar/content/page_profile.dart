//import 'package:shared/ui/placeholder/placeholder_card_short.dart';
//import 'dart:html';
//import 'dart:ui';
import 'package:incomplete_app/bottombar/popup/add_addaccount_button.dart';
import 'package:incomplete_app/bottombar/popup/add_todo_button.dart';
import 'package:incomplete_app/bottombar/popup/hero_dialog_route.dart';
import 'package:incomplete_app/bottombar/popup/custom_rect_tween.dart';
import 'package:flutter/material.dart';
import 'package:incomplete_app/auth_service.dart';
import 'package:incomplete_app/settings/screen_settings.dart';
import 'package:provider/provider.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:incomplete_app/bottombar/content/page_requests.dart';
import 'package:incomplete_app/bottombar/content/page_friends.dart';


String uName = 'Untitled User';
String uTwitter;
String uInstagram;
String uSoundcloud;
int friends, requests;

class ProfileWrapper extends StatefulWidget {
  
  final VoidCallback callbackFunc;
  ProfileWrapper(this.callbackFunc);
   @override
   _WrapperState createState() => _WrapperState(callbackFunc);
 }
 
 class _WrapperState extends State<ProfileWrapper> {
   final VoidCallback callbackFunc;
   _WrapperState(this.callbackFunc);
   @override
   Widget build(BuildContext context) {
     final FirebaseAuth auth = FirebaseAuth.instance;
     CollectionReference users = FirebaseFirestore.instance.collection('users');
     return  StreamBuilder<DocumentSnapshot>(
          stream: users.doc(auth.currentUser.uid).snapshots(),
          builder: (ctx, snapshot) {
            if (snapshot.hasError) {
              return Center(child: Text('User info is not loaded from Firebase'));
            }

            if (snapshot.connectionState == ConnectionState.waiting) {
              return Center(child: CircularProgressIndicator(strokeWidth: 5));
            }

            final info = snapshot.data; // тут доки были

            try{
            uName = info.get('username');
            }catch(Exception){uName = 'Unable to find info';}
            try{
            uTwitter = info.get('twitter');
            }catch(Exception){uTwitter = '';}
            try{
            uInstagram = info.get('instagram');
            }catch(Exception){uInstagram = '';}
            try{
            uSoundcloud = info.get('soundcloud');
            }catch(Exception){uSoundcloud = '';}
            try{
            friends = info.get('friendscount');
            }catch(Exception){friends = 0;}
            try{
            requests = info.get('pendingcount');
            }catch(Exception){requests = 0;}
            //uSoundcloud = info.get('soundcloud');

            if (snapshot.hasData) {
            return Profilepage(callbackFunc);
            }
            else {
            //return Center(child : Text("User info is not loaded from Firebase"),);
            }
   });
   }
 }


class Profilepage extends StatefulWidget {

  final VoidCallback callbackFunc;
  Profilepage(this.callbackFunc);
  @override
  _ProfilepageState createState() => _ProfilepageState(callbackFunc);
}

class _ProfilepageState extends State<Profilepage> {
  VoidCallback callbackStopTracking;
  _ProfilepageState(this.callbackStopTracking);
  void openSettings(BuildContext context)
  {
    Navigator.push(context, MaterialPageRoute(builder: (context) => SettingsWrapper()));
  }
  void showRequests(BuildContext context)
  {
    Navigator.push(context, MaterialPageRoute(builder: (context) => RequestsWrapper()));
  }
  void showFriends(BuildContext context)
  {
    Navigator.push(context, MaterialPageRoute(builder: (context) => FriendsWrapper()));
  }
  List<Container> accounts = [];
  ChoicePlatform platform = ChoicePlatform();
  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return Stack(
      fit: StackFit.expand,
      children: [
        Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              colors: [
                Color.fromRGBO(255, 255, 255, 1),
                Color.fromRGBO(216, 216, 216, 1),
              ],
              begin: FractionalOffset.bottomCenter,
              end: FractionalOffset.topCenter,
            ),
          ),
        ),
        Scaffold(
          backgroundColor: Colors.transparent,
          body: 
          SingleChildScrollView(
            physics: ClampingScrollPhysics(),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 30),
              child: Container(
              child: Column(
                mainAxisSize: MainAxisSize.max,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      IconButton(
                        icon: Icon(
                          Icons.settings,
                          color: Colors.white,
                        ),
                            onPressed: (){
                          openSettings(context);
                        }
                      ),
                      IconButton(
                        icon: Icon(
                          Icons.logout,
                          color: Colors.white,
                        ),
                            onPressed: () {
                              context.read<AuthenticationService>().signOut();
                              callbackStopTracking();
                        }
                      ),
                    ],
                  ),
                  Container(
                    height: height,
                    child: 
                    LayoutBuilder(
                      builder: (context, constraints) {
                        double innerHeight = constraints.maxHeight;
                        double innerWidth = constraints.maxWidth;
                        return 
                        Container(
                        child: Stack(
                          fit: StackFit.expand,
                          children: [
                              Positioned(
                              //top: 85,
                              //bottom: 0,
                              left: 0,
                              right: 0,
                              child: Container(
                                width: width,
                                //margin: EdgeInsets.only(bottom: 0 ),
                                margin: EdgeInsets.only(top:85),
                                decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(10),
                                  color: Colors.white,
                                  boxShadow: [
                                  BoxShadow(
                                   color: Colors.grey.withOpacity(0.5),
                                   spreadRadius: -5,
                                    blurRadius: 30,
                                    offset: Offset(0, 6), // Позиция
                                   ),
                                  ],
                                ),
                                child: Column(
                                  mainAxisSize: MainAxisSize.min,
                                  //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                  children: [
                                    SizedBox(
                                      height: 80,
                                    ),
                                Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                      children : [
                                        SizedBox(width:25),
                                        

                                        

                                        Text(
                                        uName,
                                          overflow: TextOverflow.visible,
                                          maxLines: 1,
                                        style: TextStyle(
                                          //color: Color.fromRGBO(0, 0, 0, 1),
                                          //fontFamily: 'Nunito',
                                          fontSize: 30,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                        AddTodoButton() // рифат зачем ты здесь кнопку поставил вот такую можно было проще 
                              ]
                              ),

                                    SizedBox(
                                      height: 5,
                                    ),

                                    Column(
                                      children: [
                                        Text('id: ${FirebaseAuth.instance.currentUser.uid}',style: TextStyle(fontSize: 17)),
                                        //Text('City, Country',style: TextStyle(fontSize: 17)),
                                      ],
                                    ),

                                    //SizedBox(height: 10,),

                                    Container( //рифат писал 
                                      //width: innerWidth * 0.9,
                                      margin: EdgeInsets.all(12),
                                      decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(7),
                                        color: Color(0xfff3f3f4)
                                        ),
                                      child:
                                    Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        FlatButton(
                                          onPressed: (){
                                            showFriends(context);
                                            },
                                          child: Column(
                                          children: [
                                            Text(
                                              'Friends',
                                              style: TextStyle(
                                                color: Colors.grey[700],
                                                //fontFamily: 'Nunito',
                                                fontSize: 17,
                                              ),
                                            ),
                                            /*
                                            Text(
                                              friends.toString(),
                                              style: TextStyle(
                                                color: Color.fromRGBO(
                                                    39, 105, 171, 1),
                                                //fontFamily: 'Nunito',
                                                fontSize: 25,
                                              ),
                                            ),
                                            */
                                          ],
                                        )),
                                        Padding(
                                          padding: const EdgeInsets.symmetric(
                                            horizontal: 25,
                                            vertical: 8,
                                          ),
                                          child: Container(
                                            height: 50,
                                            width: 3,
                                            decoration: BoxDecoration(
                                              borderRadius:
                                                  BorderRadius.circular(100),
                                              color: Colors.grey,
                                            ),
                                          ),
                                        ),
                                        
                                        FlatButton(
                                          onPressed: (){
                                            showRequests(context);
                                            },
                                          child: Column(
                                          children: [
                                            Text(
                                              'Requests',
                                              style: TextStyle(
                                                color: Colors.grey[700],
                                                //fontFamily: 'Nunito',
                                                fontSize: 17,
                                              ),
                                            ),/*
                                            Text(
                                              requests.toString(),
                                              style: TextStyle(
                                                color: Color.fromRGBO(
                                                    39, 105, 171, 1),
                                                //fontFamily: 'Nunito',
                                                fontSize: 25,
                                              ),
                                            ),*/
                                          ],
                                        )),
                                      ],
                                    ),
                                    ),
                  Row(children: [AccInfo(uTwitter, 0)]),
                  Row(children : [AccInfo(uInstagram, 1)]),
                  Row(children : [AccInfo(uSoundcloud, 2)]),
                  Row(children: [AddAccButton()]),
        
                                  ],
                                ),
                              ),
                            ),
                            Positioned(
                              top: 0,
                              left: 0,
                              right: 0,

                              child: Center(

                                child: Container(

                                  child: CircleAvatar(
                                    backgroundImage: AssetImage('assets/floppa.jpg'),
                                  radius: 79.0,
                                  backgroundColor: Color(0xffd2d2d2),
                                  ),
                                ),
                              ),

                            ),

                          ],
                        )
                      );
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),//
        )//
        ),
      ],
    );
  }
}

Widget AccInfo(String uAcName, int ind){

  try{uAcName.length;}catch(Excception){return Container();}
  if (uAcName.length == 0) return Container(); 
  else return Container(
                    margin: EdgeInsets.only(right:12,left:12,top:5,bottom:5),
                    decoration: BoxDecoration(
                      color: pickColor(ind),
                      borderRadius: BorderRadius.circular(8.0),
                    ),
                    //fillColor: Color(0xffBDDEB5),
                    //   
                        child: Row(
                            mainAxisSize: MainAxisSize.min,
                            //crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                                  Container(
                                    padding: EdgeInsets.only(top: 4, bottom: 4, right: 2, left: ind == 1 ? 4 : 7),
                                    child: 
                                        Icon (pickIcon(ind), color: Colors.white),
                                      
                                  ),
                                  Container( padding: EdgeInsets.only(right:8),
                                  child: Text(uAcName,
                                    //textAlign: TextAlign.center,
                                    style: TextStyle(fontWeight: FontWeight
                                        .w500,
                                        fontSize: 20,
                                        color: Colors.white),)),

                                  
                            ]
                        ),
                  );
}

IconData pickIcon(int ind){
  switch(ind){
    case 0: return (MdiIcons.twitter);    
    case 1: return (MdiIcons.instagram);
    case 2: return (MdiIcons.soundcloud);                   
  }
}

Color pickColor(int ind){
  switch(ind){
    case 0: return Color(0xFF5EA9DD);    
    case 1: return Color(0xFFC13584);
    case 2: return Color(0xFFFE5000);                   
  }
}