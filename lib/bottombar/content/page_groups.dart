//import 'package:shared/ui/placeholder/placeholder_image.dart';
//import 'dart:html';

import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:incomplete_app/bottombar/popup/CreateGroupButton.dart';
import 'package:incomplete_app/bottombar/popup/JoinGroupButton.dart';
import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:incomplete_app/bottombar/popup/custom_rect_tween.dart';
import 'package:incomplete_app/bottombar/popup/hero_dialog_route.dart';
//import 'package:incomplete_app/bottombar/popup/show_profile.dart';
import 'package:incomplete_app/bottombar/content/page_profile.dart';

Widget GroupContainer(List<Widget> res, BuildContext context, double width, String uid, VoidCallback update) {//врапер для группы  
  bool removeTapped = false;
  DocumentReference friend = FirebaseFirestore.instance.collection('groups').doc(uid);
  return StreamBuilder<DocumentSnapshot>(
                    stream: friend.snapshots(),
                    builder: (ctx, snapshot) {
                    if (snapshot.hasError) {
                    return Text('hasError');
                    }

                    if (snapshot.connectionState == ConnectionState.waiting) {
                    return Container();
                    }

                    String title, info, id;
                    List<String> people;
                    final fields = snapshot.data;
                    id = fields.id;
                    try{
                    title = fields.get('title'); 
                    }
                    catch(Exception){title = '000Del';}
                    try{
                    info = fields.get('info'); 
                    }
                    catch(Exception){info = 'The group does not exist';}
                    
                    try{
                      for(var rest in fields.get('people')){
                        people.add(rest);
                      }
                    }catch(Exception){
                      people = [];
                    }

                    if (snapshot.hasData && title != '000Del') {
                    return 

  Container(
  child: SizedBox(
    width: width*0.9,
    height: 120,
    child: Container(
      //widthFactor: 10,
      alignment: Alignment.centerLeft,
      child: RawMaterialButton(
        //shape: BoxBorder(),
        //fillColor: Color(0xffC4C4C4),
        onPressed: (){
           Navigator.of(context).push(HeroDialogRoute(builder: (context) {
            return _AddTodoPopupCard(title, info, id, people, update);
          }));
        },
        child: Center(
        child: Container(
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(10),color: Colors.white),
          child: Row(
            children: [
              Container(padding: EdgeInsets.only(left: 10),
                child: Container(
                width: 80,
                height: 80,
                decoration: BoxDecoration(color: Color(0xafC4C4C4)
                ),
                child: Center(
                  child: Container(
                    child: Icon(Icons.group, color: Colors.white, size: 40,),
                  ), 
                ),
              )),
              Row(
                children: [
                  Row( 
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: [
              Column( children: [
              Container(
                width: width*0.9 - 80 - 30,
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: Text(
                title,
                overflow: TextOverflow.visible,
                maxLines: 3,
                style: TextStyle(fontSize: 20),
                )
              )
              ]),
              Container( 
                padding: EdgeInsets.all(10),
                child: 
              Column( children: [
              //здесь были кнопки от друзей и заявок 
              ]
              )
              ),
              ]
              )
              ],
              ),
            ],
          ),
          //alignment: Alignment.center,
          //padding: EdgeInsets.all(10),
          width: width*0.9,
          height: 100,
        )
        )
      )
    )
  )
);
}
else {
  return Container();//Center(child : Text("Unable to load info"),);
}
}
);
} 

Widget Title(width) => Container(
            padding: EdgeInsets.only(top: 23, bottom: width* 0.025,left : width* 0.05,right: width* 0.05),//left: width/4, right: width/4),
            //alignment: Alignment.topCenter,
            child: FlatButton(
              onPressed: (){},
              color: Color(0xff79B7F0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(45.0),
                //side: BorderSide(color: Colors.red)
              ),
              //fillColor: Color(0xffBDDEB5),
              child: Container(
                child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 0, top: 8, bottom: 10, right: 6),
                    child: Icon(Icons.group, color: Colors.white),
                  ),
                  Container(
                    padding: EdgeInsets.only(right: 2),
                    child:
                  Text('Groups', textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.w500,fontSize: 22, color: Colors.white),)
                  )
                ]
              )),
            )
          );

          Widget TitleFriends(width) => Container(
            padding: EdgeInsets.only(top: 23, bottom: width* 0.025,left : width* 0.05,right: width* 0.05),//left: width/4, right: width/4),
            //alignment: Alignment.topCenter,
            child: FlatButton(
              onPressed: (){},
              color: Color(0xff79B7F0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(45.0),
                //side: BorderSide(color: Colors.red)
              ),
              //fillColor: Color(0xffBDDEB5),
              child: Container(
                child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Container(
                    padding: EdgeInsets.only(left: 0, top: 8, bottom: 10, right: 6),
                    child: Icon(Icons.group, color: Colors.white),
                  ),
                  Container(
                    padding: EdgeInsets.only(right: 2),
                    child:
                  Text('Members', textAlign: TextAlign.center,style: TextStyle(fontWeight: FontWeight.w500,fontSize: 22, color: Colors.white),)
                  )
                ]
              )),
            )
          );

List<Widget> pendingWidgetList(List<Widget> res, BuildContext context, List<String> uids, double width, VoidCallback callback){
    for(var x in uids){
      Widget friend = GroupContainer(res, context, width, x, callback); 
      res.add(friend);
    }
    return res;
  }
  List<Widget> membersWidgetList(List<Widget> res, BuildContext context, List<String> uids, double width, VoidCallback callback){
    for(var x in uids){
      Widget friend = FriendContainer(res, context, width, x, callback); 
      res.add(friend);
    }
    return res;
  }

class GroupsWrapper extends StatefulWidget {//врапер для всего экрана
  
  //final VoidCallback callbackFunc;
   @override
   _WrapperState createState() => _WrapperState();
 }
 
 class _WrapperState extends State<GroupsWrapper> {
    final FirebaseAuth auth = FirebaseAuth.instance;
    void updateCallback(){setState((){});}//спасло мне жизнь
   //final VoidCallback callbackFunc;
   List<String> pendingList = [];
   _WrapperState();
   @override
   Widget build(BuildContext context) {
     final FirebaseAuth auth = FirebaseAuth.instance;
     DocumentReference friends = FirebaseFirestore.instance.collection('users').doc(auth.currentUser.uid);
     return  FutureBuilder<DocumentSnapshot>(
          future: friends.get(),
          builder: (ctx, snapshot) {
            if (snapshot.hasError) {
              return Center(child: Text('Friends list is not loaded from Firebase'));
            }

            if (snapshot.connectionState == ConnectionState.waiting) {
              return Scaffold(backgroundColor: Color(0xffEBEBEB), body:Center(child: CircularProgressIndicator(strokeWidth: 5)));
            }

            final pendings = snapshot.data; // тут доки были

            //try{
            //uName = info.get('username');
            //}catch(Exception){uName = 'Unable to find info';}
            //try{
            pendingList.clear();
            for(var rest in pendings.get('groups')){
              pendingList.add(rest.toString());
            }

            if (snapshot.hasData) {
            return GroupsPage(pendingList, updateCallback);
            }
            else {
            return Center(child : Text("User info is not loaded from Firebase"),);
            }
   });
   }
 }

class GroupsPage extends StatefulWidget {
  VoidCallback callback;
  List<String> pendingList;
  GroupsPage(this.pendingList, this.callback);
  @override
  _FriendsPageState createState() => _FriendsPageState(pendingList, callback);
}

class _FriendsPageState extends State<GroupsPage> {
  List<String> pendingList;
  List<Widget> reslist = [];
  _FriendsPageState(this.pendingList, this.updateCallback);

  VoidCallback updateCallback;

  @override
  Widget build(BuildContext context) {
    
    //bool isLandscape = MediaQuery.of(context).size.aspectRatio > 1;
    var columnCount = 1;

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    reslist.add(Title(width));

    return new Scaffold(backgroundColor: Color(0xffEBEBEB), body: Container(
      child: Stack(children: [ ListView(
      scrollDirection: Axis.vertical,
      children:  
          //ListView(
          //  children: pendingWidgetList(pendingList, width)
          //),
          pendingWidgetList(reslist,context, pendingList, width, updateCallback),
          //тут были группы захардкожены
      ),
      CreateGroupButton(updateCallback),
      JoinGroupButton(updateCallback),
      ]
      )
    )
    );
  }
} 

const String _heroAddTodo = 'add-todo-hero';

//профиль 
class _AddTodoPopupCard extends StatefulWidget {
    String title, info, id;
    List<String> people;
    VoidCallback update;
    _AddTodoPopupCard(this.title, this.info, this.id, this.people, this.update);
    @override
    State createState() => _State(title, info, id, people, update);
  }

  class _State extends State<_AddTodoPopupCard> {
    String title, info, id;
    VoidCallback update;
    List<String> people;
    _State(this.title, this.info, this.id, this.people, this.update);
  /// {@macro add_todo_popup_card}
  ///const _AddTodoPopupCard({Key key}) : super(key: key);
  
  final FirebaseAuth auth = FirebaseAuth.instance;
  //final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Center(
      
        child: Hero(
          tag: _heroAddTodo,
          createRectTween: (begin, end) {
            return CustomRectTween(begin: begin, end: end);
          },
          child: Container(
             padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column( children: [
            SizedBox(height: height/2 - 60),
          Row(
           //mainAxisSize: MainAxisSize.max,
           mainAxisAlignment: MainAxisAlignment.center,
           mainAxisSize: MainAxisSize.max,
         children: [
           
          Material(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),

          child: Container( margin: EdgeInsets.symmetric(horizontal: 3,vertical: 5),
            child: Column(children: [
            Container(margin: EdgeInsets.only(top:13,left:17,right:17,bottom:13),
            child: Column(
            children: [
              Container(width:MediaQuery.of(context).size.width*0.7,
                child:
              Text(
              //widget.nearestUsers[curChosenUser].name,
              title,//'No user  info'
                overflow: TextOverflow.visible,
                maxLines: 3,
              style: TextStyle(fontSize: 23, fontWeight: FontWeight.w500),
              ),
              ),
              
              //onLongPress: (){Clipboard.setData(new ClipboardData(text: data);},
              SizedBox(height: 8),
              Container(
                width: MediaQuery.of(context).size.width * 0.7,
              child: SelectableText(
              id,//'No user  info'
              style: TextStyle(fontSize: 15, color: Colors.grey),
              )
              ),
              
              Container(
                height: info.length == 0 ? 0 : null,
                width: MediaQuery.of(context).size.width * 0.7,
              child: SelectableText(
              info,
              minLines: 1,
              maxLines : 4,
              //'No user  info'
              style: TextStyle(fontSize: 19),
              )),
            ])
            ),

            //кнопки ниже
            Row( children : [
              Container(
              margin: EdgeInsets.all(6),
              //padding: EdgeInsets.all(),
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(7), color: Color(0xff79B7F0)),
            child: FlatButton(onPressed: () {setState((){
              FirebaseFirestore.instance.collection('groups').doc(id).update({
                'people' : FieldValue.arrayRemove([FirebaseAuth.instance.currentUser.uid]),
              });
              FirebaseFirestore.instance.collection('users').doc(FirebaseAuth.instance.currentUser.uid).update({
                'groups' : FieldValue.arrayRemove([id]),
              });
              update();
            });
            }, 
            child: Container(
              //width: MediaQuery.of(context).size.width - 80,
              
              child: Text('Leave group', style: TextStyle(color: Colors.white, fontSize: 19),),            
            )
            )
            ), 
            Container(
              margin: EdgeInsets.all(6),
              //padding: EdgeInsets.all(),
              decoration: BoxDecoration(borderRadius: BorderRadius.circular(7), color: Color(0xff79B7F0)),
            child: FlatButton(onPressed: () {
              showRequests(context, id);
            }, 
            child: Container(
              //width: MediaQuery.of(context).size.width - 80,
              
              child: Text('Show members', style: TextStyle(color: Colors.white, fontSize: 19),),            
            )
            )
            )
            ]
            
            )
            //AccInfo(twt, 0),
            //AccInfo(ins, 1),
            //AccInfo(snc, 2)
              
          ]))
          ),
         ]
         ),
         
         //данные какого то аккаунта
         
         ]
          )
         ),
        ),
    );
  }
}


//Гигантская копипаста спасшая жизнь

void showRequests(BuildContext context, String id)
{
  Navigator.push(context, MaterialPageRoute(builder: (context) => FriendsWrapper(id)));
}

class FriendsWrapper extends StatefulWidget {
  String id;
  FriendsWrapper(this.id);
  //final VoidCallback callbackFunc;
   @override
   _Wrapper2State createState() => _Wrapper2State(id);
 }
 
 class _Wrapper2State extends State<FriendsWrapper> {
    String id;
    _Wrapper2State(this. id);
    final FirebaseAuth auth = FirebaseAuth.instance;
    void updateCallback(){setState((){});}//спасло мне жизнь
   //final VoidCallback callbackFunc;
   List<String> pendingList = [];
   @override
   Widget build(BuildContext context) {
     final FirebaseAuth auth = FirebaseAuth.instance;
     DocumentReference friends = FirebaseFirestore.instance.collection('groups').doc(id);
     return  FutureBuilder<DocumentSnapshot>(
          future: friends.get(),
          builder: (ctx, snapshot) {
            if (snapshot.hasError) {
              return Center(child: Text('Friends list is not loaded from Firebase'));
            }

            if (snapshot.connectionState == ConnectionState.waiting) {
              return Scaffold(backgroundColor: Color(0xffEBEBEB), body:Center(child: CircularProgressIndicator(strokeWidth: 5)));
            }

            final pendings = snapshot.data; // тут доки были

            //try{
            //uName = info.get('username');
            //}catch(Exception){uName = 'Unable to find info';}
            //try{
            pendingList.clear();
            for(var rest in pendings.get('people')){
              pendingList.add(rest.toString());
            }

            if (snapshot.hasData) {
            return Friends2Page(pendingList, updateCallback);
            }
            else {
            return Center(child : Text("User info is not loaded from Firebase"),);
            }
   });
   }
 }

class Friends2Page extends StatefulWidget {
  VoidCallback callback;
  List<String> pendingList;
  Friends2Page(this.pendingList, this.callback);
  @override
  _Friends2PageState createState() => _Friends2PageState(pendingList, callback);
}

class _Friends2PageState extends State<Friends2Page> {
  List<String> pendingList;
  List<Widget> reslist = [];
  _Friends2PageState(this.pendingList, this.updateCallback);

  VoidCallback updateCallback;

  @override
  Widget build(BuildContext context) {
    
    //bool isLandscape = MediaQuery.of(context).size.aspectRatio > 1;
    var columnCount = 1;

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    reslist.add(TitleFriends(width));

    return new Scaffold(backgroundColor: Color(0xffEBEBEB), body: Container(
      child: ListView(
      scrollDirection: Axis.vertical,
      children:  
          //ListView(
          //  children: pendingWidgetList(pendingList, width)
          //),
          membersWidgetList(reslist,context, pendingList, width, updateCallback),
          //тут были заявки или друзья захардкожены
      )
    )
    );
  }
} 

class _FriendPopupCard extends StatefulWidget {
    String name, id, twt, ins, snc, email;
    bool busy;
    _FriendPopupCard(this.busy, this.email, this.name, this.id, this.twt, this.ins, this.snc);
    @override
    State createState() => _FriendState(busy, email, name, id, twt, ins, snc);
  }

  class _FriendState extends State<_FriendPopupCard> {
    String name, id, twt, ins, snc, email;
    bool busy;
    _FriendState(this.busy, this.email, this.name, this.id, this.twt, this.ins, this.snc);
  /// {@macro add_todo_popup_card}
  ///const _AddTodoPopupCard({Key key}) : super(key: key);
  
  final FirebaseAuth auth = FirebaseAuth.instance;
  //final TextEditingController _controller = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    return Center(
      
        child: Hero(
          tag: _heroAddTodo,
          createRectTween: (begin, end) {
            return CustomRectTween(begin: begin, end: end);
          },
          child: Container(
             padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column( children: [
            SizedBox(height: height/2 - 60),
          Row(
           //mainAxisSize: MainAxisSize.max,
           mainAxisAlignment: MainAxisAlignment.center,
           mainAxisSize: MainAxisSize.max,
         children: [
           
          Material(
            shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(12)),

          child: Container( margin: EdgeInsets.symmetric(horizontal: 3,vertical: 5),
            child: Column(children: [
            Container(margin: EdgeInsets.only(top:13,left:17,right:17,bottom:13),
            child: Column(
            children: [
              Text(
              //widget.nearestUsers[curChosenUser].name,
              name,//'No user  info'
              style: TextStyle(fontSize: 20),
              ),
              SizedBox(height: busy ? 6 : 0),
              Container(
              height: busy ? null : 0,
              child: SelectableText(
              email,
              style: TextStyle(fontSize: 15,),
              )),
              Container(
                height: busy ? null : 0,
                child: Text('User turned on Busy mode',
                  style: TextStyle(fontSize: 15, color: Colors.grey),
                )
              )
            ])
            ),

            //аккаунты ниже
            
            AccInfo(twt, 0),
            AccInfo(ins, 1),
            AccInfo(snc, 2)
              
          ]))
          ),
         ]
         ),
         
         //данные какого то аккаунта
         
         ]
          )
         ),
        ),
    );
  }
}

//контейнер в котором мембер
Widget FriendContainer(List<Widget> res, BuildContext context, double width, String uid, VoidCallback update) {//врапер для мембера 
  bool removeTapped = false;
  DocumentReference friend = FirebaseFirestore.instance.collection('users').doc(uid);
  return StreamBuilder<DocumentSnapshot>(
                    stream: friend.snapshots(),
                    builder: (ctx, snapshot) {
                    if (snapshot.hasError) {
                    return Text('hasError');
                    }

                    if (snapshot.connectionState == ConnectionState.waiting) {
                    return Container();
                    }

                    String name, id, twt, ins, snc;
                    final fields = snapshot.data;
                    id = fields.id;
                    bool busy;
                    String email = fields.get('email');

                    try{
                    busy = fields.get('busy'); 
                    }
                    catch(Exception){busy = false;}

                    try{
                    name = busy? fields.get('busyname') : fields.get('username'); 
                    }
                    catch(Exception){name = 'Name is empty';}
                    try{
                    twt = fields.get('twitter'); 
                    }
                    catch(Exception){twt = '';}
                    try{
                    ins = fields.get('instagram'); 
                    }
                    catch(Exception){ins = '';}
                    try{
                    snc = fields.get('soundcloud'); 
                    }
                    catch(Exception){snc = '';}

                    if (snapshot.hasData) {
                    return 

  Container(
  child: SizedBox(
    width: width*0.9,
    height: 120,
    child: Container(
      //widthFactor: 10,
      alignment: Alignment.centerLeft,
      child: RawMaterialButton(
        //shape: BoxBorder(),
        //fillColor: Color(0xffC4C4C4),
        onPressed: (){
           Navigator.of(context).push(HeroDialogRoute(builder: (context) {
            return _FriendPopupCard(busy, email, name, id, twt, ins, snc);
          }));
        },
        child: Center(
        child: Container(
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(10),color: Colors.white),
          child: Row(
            children: [
              Container(padding: EdgeInsets.only(left: 10),
                child: Container(
                width: 80,
                height: 80,
                decoration: BoxDecoration(color: Color(0xafC4C4C4)
                ),
                child: Center(
                  child: Container(
                    child: Icon(Icons.group, color: Colors.white, size: 40,),
                  ), 
                ),
              )),
              Row(
                children: [
                  Row( 
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: [
              Column( children: [
              Container(
                width: width*0.9 - 80 - 35 - 30,
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
              child: Text(
                name,
                style: TextStyle(fontSize: 20),
                )
              )
              ]),
              Container( 
                padding: EdgeInsets.all(10),
                child: 
              Column( children: [
                //кнопки для заявок участникам
              SizedBox(height:45),
              Container(
                height: 35,
                width: 35,
                decoration: BoxDecoration(color: Color(0xff6AC26E), borderRadius: BorderRadius.circular(4)),
                child:
              IconButton(
                iconSize: 20,
                onPressed: (){
                  removeTapped = !removeTapped;
                  //res.remove(id);
                  update();
                  FirebaseFirestore.instance.collection('users').doc(id).update({
                    //FirebaseFirestore.instance.collection('users').doc(id).get('friends').then((value) => null)
                    'pending':  FieldValue.arrayUnion([FirebaseAuth.instance.currentUser.uid]),
                    //'pendingcount': FieldValue.increment(-1),
                  }
                  );
                },
                icon: Icon(Icons.person_add),
                color: Colors.white,
              )),
              ]
              )
              ),
              ]
              )
              ],
              ),
            ],
          ),
          //alignment: Alignment.center,
          //padding: EdgeInsets.all(10),
          width: width*0.9,
          height: 100,
        )
        )
      )
    )
  )
);
}
else {
  return Center(child : Text("Unable to load info"),);
}
}
);
} 