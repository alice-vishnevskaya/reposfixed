import 'dart:async';
import 'dart:ui';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:incomplete_app/auth_service.dart';

import 'package:background_locator/location_dto.dart';
import 'package:incomplete_app/bottombar/content/page_profile.dart';
import 'package:incomplete_app/locator/location_service_repository.dart';

import 'package:background_location/background_location.dart';

import 'package:carousel_slider/carousel_slider.dart';

import 'package:material_design_icons_flutter/material_design_icons_flutter.dart';
import 'package:incomplete_app/bottombar/bar.dart';


  class Wrapper extends StatefulWidget {
  //LocationDto data = LocationServiceRepository().current;
  Wrapper(this.latitude, this.longtitude, this.locate, this.unlocate);
  VoidCallback locate;
  VoidCallback unlocate;
  StringReference latitude, longtitude;

   @override
   _WrapperState createState() => _WrapperState(latitude, longtitude, locate, unlocate);
 }
 
 class _WrapperState extends State<Wrapper> {
   StringReference latitude, longtitude;
   VoidCallback locate;
   VoidCallback unlocate;
   void refresh(){setState((){});}

   List<UserInfo> nearestUsers = [];

   _WrapperState(this.latitude, this.longtitude, this.locate, this.unlocate);
   @override
   Widget build(BuildContext context) {
     //try{double.parse(latitude.value);}catch(Exception){sleep(Duration(seconds: 5));}
     CollectionReference users = FirebaseFirestore.instance.collection('users');
     return  StreamBuilder<QuerySnapshot>(
          stream: users.snapshots(),
          builder: (ctx, snapshot) {
            if (snapshot.hasError) {
              return Center(child: Text('Something went wrong'));
            }

            if (snapshot.connectionState == ConnectionState.waiting || longtitude.value.length == 0) {
              return Center(child: CircularProgressIndicator(strokeWidth: 5));
            }

            bool peopleAround(QueryDocumentSnapshot<Object> element){
              //Timer timer;
              double lat0;
              double lon0;
              //try{double.parse(latitude.value);}catch(Exception){}
              
              double lat = element.get('Location')[0];
              double lon = element.get('Location')[1];
              //sleep(Duration(seconds: 5));
              lat0 = double.parse(latitude.value);
              lon0 = double.parse(longtitude.value);

              return
              ((lat - lat0)*1000).round().abs() <= 1
              && 
              ((lon - lon0)*1000).round().abs() <= 1;
            }
            
            bool notBusy(QueryDocumentSnapshot<Object> element){
              bool res;
              try{res = !element.get('busy');}
              catch(Exception){res = true;}
              return res;
            }

            final documents = snapshot.data.docs.where((element) => (peopleAround(element) && notBusy(element)));
            documents.forEach((element){
                //nearestUsers.add(new UserInfo(element.get('username').toString(), 
                //'assets/floppa.png', element.get('twitter').toString(), element.get('instagram').toString(),
                // element.get('soundcloud').toString()));
                String id, usn, twt, ins, snc;
                id = element.id;
                try{
                usn = element.get('username').toString();
                }catch(Exception){usn = '';}
                try{
                  twt = element.get('twitter').toString();
                }catch(Exception){twt = '';}
                try{
                ins = element.get('instagram').toString();
                }catch(Exception){ins = '';}
                try{
                  snc = element.get('soundcloud').toString();
                }catch(Exception){snc = '';}
                nearestUsers.add(
                  new UserInfo(
                    id,
                    usn,
                    'assets/floppa.jpg',
                    twt,
                    ins,
                    snc,
                    //element.get('instagram').toString(),
                    //element.get('soundcloud').toString()
                  )
                );
            });
              if (snapshot.hasData) {
                return HomePage(latitude, longtitude, locate, refresh, nearestUsers);
              }
              else {
     return Center(child : Text("No people around"),);
     }
   });
   }
 }

 
class HomePage extends StatefulWidget {
  StringReference latitude, longtitude;
  VoidCallback locate;
  VoidCallback unlocate;
  List<UserInfo> nearestUsers;
  HomePage(this.latitude, this.longtitude, this.locate, this.unlocate, this.nearestUsers);
  @override
  _HomePageState createState() => _HomePageState(latitude, longtitude, locate, unlocate, nearestUsers);

}

class _HomePageState extends State<HomePage> {

  final FirebaseAuth auth = FirebaseAuth.instance;

  StringReference latitude, longtitude;
  VoidCallback locate;
  VoidCallback unlocate;
  List<UserInfo> nearestUsers;

  _HomePageState(this.latitude, this.longtitude, this.locate, this.unlocate, this.nearestUsers);

  int curChosenUser=0;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    //onStart();
    final height = MediaQuery
        .of(context)
        .size
        .height;
    final width = MediaQuery
        .of(context)
        .size
        .width;

    if (nearestUsers.isEmpty) return Scaffold(backgroundColor: Color(0xffEBEBEB),
    body: Center(child: Text('There are no unbusy users nearby')));
    
    return Scaffold(
        backgroundColor: Color(0xffEBEBEB),
        body: Stack(children: [

          Container(
              margin: EdgeInsets.symmetric(vertical: width / 8)
          ),

          

          Container(
            width: width,
            height: height,
            child: Container(

            ),
          ),

          
          Container(
           //alignment: Alignment.center,
           padding: EdgeInsets.only(top:100),
           //margin: EdgeInsets.symmetric(vertical: 100),
           child: CarouselSlider(
            //itemExtent: 220,
            //diameterRatio: 3.5,
            //offAxisFraction: 0,
            options: CarouselOptions(
            //physics: FixedExtentScrollPhysics(),
             //useMagnifier: true,
             //magnification: 1.2,
             height:200,
             scrollDirection: Axis.horizontal,
             onPageChanged: (index, reason)=> {
               setState(() {
                 curChosenUser = index;
                 print(nearestUsers[curChosenUser].name);
               })
             },
            ),
            items: buildUserIcons(nearestUsers)
            ),
          ),

          Container(
              padding: EdgeInsets.only(top: 23),
              //margin: EdgeInsets.symmetric(horizontal: 20),
              //alignment: Alignment.topCenter,

              
                //fillColor: Color(0xffBDDEB5),
                
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  //mainAxisSize: MainAxisSize.min,
                  children: [Container(
                    padding: EdgeInsets.only(bottom: 50, top: 12,left: 14, right: 14),
                    decoration: BoxDecoration(
                      color: Color(0xff79B7F0),
                      borderRadius: BorderRadius.circular(20.0),
                      boxShadow: [BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 7,
                    blurRadius: 28,
                    offset: Offset(0, 15),
                  )
                  ]
                    ),
                    child: Row(
                        mainAxisSize: MainAxisSize.min,
                        
                        children: [
                          // Container(
                          //   padding: EdgeInsets.only(left: 0, top: 13, bottom: 13, right: 2),
                          //   child: Icon(Icons.place_outlined, color: Colors.white),
                          // ),
                          Container(
                              //padding: EdgeInsets.only(right: 2),
                              child:
                              Text('People around', textAlign: TextAlign.center,
                                style: TextStyle(fontWeight: FontWeight.w500,
                                    fontSize: 23,
                                    color: Colors.white),)
                          )
                        ]
                    ))]),
              
          ),

          Container(
              padding: EdgeInsets.only(top: 66),
              //alignment: Alignment.topCenter,
              child: Container(
                  decoration: BoxDecoration(
                  ),

                  child: Row( mainAxisAlignment: MainAxisAlignment.center,
                  children: [Container(
                    
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(45.0),
                    ),
                    //fillColor: Color(0xffBDDEB5),
                    //   
                        child: Row(

                          mainAxisSize: MainAxisSize.min,
                            children: [
                                  Container(
                                    padding: EdgeInsets.only(top: 13, bottom: 13, right: 2, left: 13),
                                    child: Icon(
                                        Icons.place_outlined, color: Colors.black),
                                  ),
                                  Container( padding: EdgeInsets.only(right:17),
                                  child: Text("${latitude.value} ${longtitude.value}",
                                    //textAlign: TextAlign.center,
                                    style: TextStyle(fontWeight: FontWeight
                                        .w500,
                                        fontSize: 20,
                                        color: Colors.black),))
                                  
                            ]
                        ),
                  )]))
          ),
         
         //микро профиль с инфо
         Stack (children: [
           Container(
             padding: EdgeInsets.symmetric(horizontal: 20),
          child: Column( children: [
          Row(
           //mainAxisSize: MainAxisSize.max,
           mainAxisAlignment: MainAxisAlignment.center,
           mainAxisSize: MainAxisSize.max,
         children: [
          Container(
          //height:160,
          //width: 150,
          margin: EdgeInsets.only(top: 305),

          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(12),
            boxShadow: [
                                  BoxShadow(
                                   color: Colors.grey.withOpacity(0.5),
                                   spreadRadius: -5,
                                    blurRadius: 30,
                                    offset: Offset(0, 6), // Позиция
                                   ),
                                  ],
          ),
          child: Container( margin: EdgeInsets.symmetric(horizontal: 3,vertical: 5),
            child: Column(children: [
            Container(margin: EdgeInsets.only(top:25,left:17,right:17,bottom:13),
            child: Text(
              //widget.nearestUsers[curChosenUser].name,
              "Nearby",
              style: TextStyle(fontSize: 20),
              )
            ),

            //аккаунты ниже
            
            AccInfo(nearestUsers[curChosenUser].twitter, 0),
            AccInfo(nearestUsers[curChosenUser].instagram, 1),
            AccInfo(nearestUsers[curChosenUser].soundcloud, 2)
              
          ]))
          ),
         ]
         ),
         
         //данные какого то аккаунта
         
         ]
          )
         ),

          Row(
            mainAxisAlignment: MainAxisAlignment.center,
             
            children: [
          Container(
          height:40,
          //width: 150,
          margin: EdgeInsets.only(top: 285),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(20),
            boxShadow: [BoxShadow(
                    color: Colors.grey.withOpacity(0.5),
                    spreadRadius: 2,
                    blurRadius: 28,
                    offset: Offset(0, 8),
                  )
            ]
          ),
          child: Column(children: [
            Container(margin: EdgeInsets.symmetric(horizontal:15,vertical:10),
            child: Text(nearestUsers[curChosenUser].name,style: TextStyle(
              fontSize: 20,
            ),)),
          ],)
        ),
        
        ])
        ])
        ,

        //кнопка для обновления
          Container(
            width: width,
            height: height,
            child: Container(
              //width: 200,
                margin: EdgeInsets.symmetric(horizontal: 20, vertical: 30),
                alignment: Alignment.bottomCenter,
                child: Row( mainAxisAlignment: MainAxisAlignment.spaceBetween, 
                  children: [
                  new RawMaterialButton(
                  shape: new CircleBorder(
                  ),
                  fillColor: Color(0xff6AC26E),
                  //elevation: 50,
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    child: Icon(
                      Icons.person_add_alt_1,
                      color: Colors.white,
                      size: width / 10,
                    ),
                  ),
                  onPressed: () {
                    //unlocate();
                    //заявка
                    FirebaseFirestore.instance.collection('users').doc(nearestUsers[curChosenUser].id).update(
                      {  
                        'pending': FieldValue.arrayUnion([auth.currentUser.uid]),
                        'pendingcount': FieldValue.increment(1)
                      }
                    );
                    debugCallback("Request has been sent");
                  },
                ),
                SizedBox(height: 20),
                new RawMaterialButton(
                  shape: new CircleBorder(
                  ),
                  fillColor: Color(0xff8E8CFE),
                  //elevation: 50,
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                    child: Icon(
                      Icons.refresh,
                      color: Colors.white,
                      size: width / 10,
                    ),
                  ),
                  onPressed: () {
                    unlocate();
                  },
                )
                ])
            ),
          ),

        ]
        )
    );
  }

  List<Widget> buildUserIcons(List<UserInfo> users)
  {
        List<Widget> list = [];

         

         for(int i=0;i<users.length;i++)
           {
             print(users[i].getIcon());
             list.add(ClipRRect(
             borderRadius: BorderRadius.circular(15),
             child: Image(image: AssetImage(users[i].icon),),
             ),

             );
           }
         return list;
  }

}

class UserInfo
{
  String id = "null";
  String name = "null";
  String icon = "null";
  String twitter = "null";
  String instagram = "null";
  String soundcloud = "null";
  UserInfo(this.id, String name,String img, String twitter, String instagram, String soundcloud)
  {
         this.name = name;
         icon = img;
         this.twitter = twitter;
         this.instagram = instagram;
         this.soundcloud = soundcloud;
  }

  String getIcon()
  {
     return icon;
  }
}

void debugCallback(String text)
{
  Fluttertoast.showToast(
      msg: text,
      toastLength: Toast.LENGTH_SHORT,
      gravity: ToastGravity.BOTTOM,
      timeInSecForIos: 1,
      backgroundColor: Colors.lightBlue,
      textColor: Colors.white
  );
}