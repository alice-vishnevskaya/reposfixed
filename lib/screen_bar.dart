import 'package:flutter/material.dart';
import 'bottombar/bar.dart';


class ScreenBar extends StatefulWidget {

  @override
  _ScreenBarState createState() => _ScreenBarState();
}

class _ScreenBarState extends State<ScreenBar> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: BubbleTabBar(),
    );
  }
}