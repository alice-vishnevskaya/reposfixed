import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:incomplete_app/screen_bar.dart';
import 'package:incomplete_app/screen_register.dart';
import 'incompletewidgets/EditText.dart';
import 'incompletewidgets/IncompleteButton.dart';
import 'incompletewidgets/move.dart';

class ScreenLogin extends StatefulWidget {
  @override
  _ScreenRegisterState createState() => _ScreenRegisterState();
}

List<Widget> children = [
                  
];

bool reg = true; 

Widget temp;

class _ScreenRegisterState extends State<ScreenLogin> {

  void oformitb() 
  { 
    if (reg) 
    setState(
      ()
      {
        //regSwap();
      }
    );
    else 
    {
      Navigator.push(context, MaterialPageRoute(builder: (context) => ScreenBar()));
    }
    reg = false;
  }

  void getBack() 
  { 
    if (!reg) 
    setState(
      ()
      {
       //regSwap();
      }
    );
    reg = true;
  }

  @override
  Widget build(BuildContext context) {

    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;

    return Scaffold(
    //   appBar: AppBar(
    //   backgroundColor: Colors.white, // status bar color
    //   brightness: Brightness.dark, // status bar brightness
    // ),
      body: Container(
        height : height,
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Stack(
        
          //crossAxisAlignment: CrossAxisAlignment.center,
          //mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[

            Container(
              padding: EdgeInsets.only(right: width/13, left: width/13),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.center,
                
                children: <Widget>[

                  Text('meetem',
                    style: TextStyle(
                      fontSize: width/6,
                      fontWeight: FontWeight.w400  
                    ),
                  ),

                  EditText('Username', ),

                  EditText('Password', isPassword: true),

                  Container(
                    height: !reg ? null : 0,
                    child:
                    EditText('Repeat password', isPassword: true),
                  ),
                  
                  Align(
                    alignment: Alignment.topRight,
                    child:
                  Column(
                    //mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                    Container(
                    height: reg ? null : 0,
                    margin: EdgeInsets.symmetric(horizontal : 5),
                    child: Text('Forgot the password?',
                      //textWidthBasis: TextWidthBasis.longestLine,
                      style: TextStyle(
                        //color: Color(0xfff3f3f4),
                        fontSize: 15, 
                      ),
                    ),
                    ),

                    SizedBox(
                        height: reg ? 10 : 0,
                    ),

                    ]
                  ),
                  ),

                  SizedBox(
                    height: 5,
                  ),

                  // RaisedButton(
                  //   child: Text('Register'), 
                  //   onPressed: null,            
                  // ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    //mainAxisSize: MainAxisSize.max,
                    //crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                      IncompleteButton('Log in', size : reg ? width/13*11/3 : 0, 
                        todo: (){
                          //переход на экран с баром
                          //move(context, ScreenBar());
                          Navigator.push(context, MaterialPageRoute(builder: (context) => ScreenBar()));
                        } 
                      ),
                      ]
                      ),

                      IncompleteButton('<', size : !reg ? width*0.178 : 0, iconind: !reg ? 1 : null, 
                        todo: (){
                          getBack();
                        } 
                      ),

                      SizedBox(width : 10),

                      Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                      IncompleteButton('Register', size :  width/13*11/3,  
                      todo: (){
                        // Navigator.push(context,
                        // MaterialPageRoute(builder: (context) => ScreenRegister()),
                        // )
                        oformitb();
                        // );
                        // setState(_ScreenRegisterState);
                        } 
                      ),
                      ]
                      ),
                    ]
                  ),
                ]
              )
            ),

          ],
      )
      ),
    ); 
  }
}

