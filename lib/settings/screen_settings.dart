//import 'dart:html';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:incomplete_app/themes.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class SettingsWrapper extends StatefulWidget {
  @override
  _SettingsWrapperState createState() => _SettingsWrapperState();
}

class _SettingsWrapperState extends State<SettingsWrapper> {
  @override
  Widget build(BuildContext context) {
    return FutureBuilder<DocumentSnapshot>(
      future: FirebaseFirestore.instance.collection('users').doc(FirebaseAuth.instance.currentUser.uid).get(),
      builder: (ctx, snapshot){
        if (snapshot.hasError) {
          return Center(child: Text('User info is not loaded from Firebase'));
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Center(child: CircularProgressIndicator(strokeWidth: 5));
        }

        bool isBusy;
        try{isBusy = snapshot.data.get('busy');}
        catch(Exception){isBusy = false;}

        if (snapshot.hasData) {
          return ScreenSettings(isBusy);
        }
        else {
          return Center(child : Text("Something went wrong"),);            
        }

      }
    );
  }
}

class ScreenSettings extends StatefulWidget
{
  bool isBusy;
  ScreenSettings(this.isBusy);
  @override
  _SettingsState createState() => _SettingsState(isBusy);
}

class _SettingsState extends State<ScreenSettings> {
  bool isBusy = false;
  Language lang = Language.English;
  _SettingsState(this.isBusy);
  
  String langToString(Language lang)
  {
    switch(lang)
    {
      case Language.English: return "English";
      //case Language.Russian: return "None";
    }
    return "English";
  }
  
  @override
  Widget build(BuildContext context) {
    return new Scaffold(backgroundColor: Color(0xffEBEBEB),
      body:Column(mainAxisAlignment: MainAxisAlignment.center,
          children:[
      Container(
        padding: EdgeInsets.symmetric(horizontal: 16, vertical: 10),
        margin: EdgeInsets.symmetric(vertical: 10),
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30),
            color:Color(0xff79B7F0)
        ),
        child:
        Text('Settings',
          style: TextStyle(
            fontWeight: FontWeight.w500,
            fontSize: 20,
            color: Colors.white
          ),
        )),

        Container(
        margin: EdgeInsets.symmetric(horizontal: 20),
     decoration: BoxDecoration(
      borderRadius: BorderRadius.circular(15),
      color: Colors.white
      ),
      child:
      Container(margin: EdgeInsets.symmetric(horizontal: 20, vertical: 10),
      child:
      Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
            Text('  Busy mode',
              style: TextStyle(
                fontSize: 18,
              ),
            ),
            Container(child:
            Row(children:
            [
              CupertinoSwitch(value: isBusy,
                trackColor :Color(0xFFFF6A6A),
                activeColor: Colors.blue,
                onChanged:(bool newValue) {
              setState(() {
                isBusy = newValue;
                FirebaseFirestore.instance.collection('users').doc(FirebaseAuth.instance.currentUser.uid).update({
                  'busy' : isBusy,
                });
              });
            }
            ),
              Container(child: Text((isBusy)?'On':'Off',
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.normal,
                ),
              ),
              width: 23,
              ),
            ],
            ),
            ),

          ],),
          Row(mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text('  Language',
                style: TextStyle(
                  color: Colors.grey,
                  fontSize: 18,
                  fontWeight: FontWeight.normal,
                ),
              ),
              new DropdownButton<Language>(
                value: lang,
                focusColor: Colors.grey,
                elevation: 16,
                underline: Container(
                  height: 0,
                  color: Colors.deepPurpleAccent,
                ),
                items: Language.values.map((Language lang) {
                  return new DropdownMenuItem<Language>(
                    value: lang,
                    child: new Text(langToString(lang)),
                  );
                }).toList(),
                onChanged: (Language value) {
                  setState(() {
                    //lang = value;
                  });
                },
              )

            ],
          ),
          Container(alignment: Alignment.centerLeft,
              child: TextButton(onPressed: (){}, child:
              Text("Security",
                  style: TextStyle(
                    color: Colors.grey,
                    fontWeight: FontWeight.normal,
                    fontSize: 18,
              )))
          ),
          Container(alignment: Alignment.centerLeft,
              child: TextButton(onPressed: (){}, child:
              Text("About",
                  style: TextStyle(
                    color: Colors.grey,
                    fontSize: 18,
                    fontWeight: FontWeight.normal,
                  )))
          ),
        ],
      )
      )
    )
      ])
    );
  }
}

enum Language
{
  English
}